import React, {useState, useEffect} from 'react';
import axios from 'axios';


function PrivateToDoList() {

    const contentRef = React.createRef();

    const contentArr = [];
    const [contents, setContents] = useState([]);

    useEffect(() => {

        axios.get(`http://127.0.0.1:8000/api/user/private/${Number(sessionStorage.getItem("user_id"))}`)
        .then(response => {
            for(const data of response.data)
            {
                contentArr.push(data);
            }
            setContents(contentArr);
            console.log('success', response);
        })
        .catch(error => {
            console.log('error', error);
        });
    }, []);

    const handleCreate = () => {

        axios.post(`http://127.0.0.1:8000/api/user/private/${Number(sessionStorage.getItem("user_id"))}`, {content: contentRef.current.value})
        .then(response => {
            setContents(oldContents => [...oldContents, {id: response.data.result.id, content: response.data.result.content}]);
            document.getElementById('todo-list').value = '';
            console.log('success', response);
        })
        .catch(error => {
            console.log('error', error);
        });
    }

    const handleEdit = (id, index) => {

        if(document.getElementById(`btn-${id}`).innerText === 'Edit')
        {
            document.getElementById(`content-${id}`).disabled = false;
            document.getElementById(`btn-${id}`).innerText = 'Done';
        }
        else
        {
            document.getElementById(`content-${id}`).disabled = true;
            document.getElementById(`btn-${id}`).innerText = 'Edit';

            const newContent = document.getElementById(`content-${id}`).value;

            axios.put(`http://127.0.0.1:8000/api/user/private/${Number(sessionStorage.getItem("user_id"))}/${id}`, {content: newContent})
            .then(response => {
                let temp = contents;
                temp[index] = newContent;
                console.log('success', response);
            })
            .catch(error => {
                console.log('error', error);
            });
        }
    }

    const handleDelete = (id) => {

        axios.delete(`http://127.0.0.1:8000/api/user/private/${Number(sessionStorage.getItem("user_id"))}/${id}`)
        .then(response => {
            window.location.reload();
            console.log('success', response);
        })
        .catch(error => {
            console.log('error', error);
        });
    }

    useEffect(() => {

        console.log(contents);
    }, [contents]);

    const myData = contents.map((val,index) => {

        return (
            <div className="d-flex justify-content-between mb-3">
                <input type="text" className="form-control" id={`content-${val.id}`} defaultValue={val.content} disabled/>
                <div className="d-flex">
                    <button type="button" className="btn btn-sm btn-primary" onClick={() => handleEdit(val.id, index)}> <span id={`btn-${val.id}`}> Edit </span> </button>
                    <button type="button" className="btn btn-sm btn-danger" onClick={() => handleDelete(val.id)}> Delete </button>
                </div>
            </div>
        )
    });

    return (
        <div className="container w-50">
            <form className="mt-5 mb-5">
                <h3 className="text-center"><h3>Private To-Do List</h3></h3>
                <div className="d-flex">
                    <input type="text" className="form-control" ref={contentRef} id="todo-list" placeholder="Enter text..." />
                    <button type="button" className="btn btn-secondary" onClick={handleCreate}>Create</button>
                </div>
            </form>
            <div>
                {myData}
            </div>
        </div>
    )
}

export default PrivateToDoList;