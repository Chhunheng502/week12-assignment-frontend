import {Link} from 'react-router-dom';
import Auth from './Auth';
import { useHistory } from "react-router-dom";

function Header() {

    let history = useHistory();

    const handleLogout = () => {

        Auth.logout(() => {
            sessionStorage.removeItem("is_authenticated");
            history.push('/');
        });
    }

    return (
        <div className="d-flex justify-content-between">
            <div className="d-flex">
                <div> <Link to="/public" style={{textDecoration:"none"}}> <button type="button" className="btn"> Public </button> </Link> </div>
                <div> <Link to="/private" style={{textDecoration:"none"}}> <button type="button" className="btn"> Private </button> </Link> </div>
            </div>
            <div> <button type="button" className="btn" onClick={handleLogout}> Logout </button> </div>
        </div>
    )
}

export default Header;