import React from 'react';
import axios from 'axios';

function RegistrationForm(props) {

    const loginStyle = {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)"
    };

    const nameRef = React.createRef();
    const emailRef = React.createRef();
    const passRef = React.createRef();
    const repeated_passRef = React.createRef();

    const handleRegister = () => {

        if(passRef.current.value !== repeated_passRef.current.value)
        {
            alert("Wrong Password");
        }
        else
        {
            const myObj = {
                name: nameRef.current.value,
                email: emailRef.current.value,
                password: passRef.current.value
            };
    
            axios.post('http://127.0.0.1:8000/api/user/register', myObj)
            .then(response => {
                props.history.push('/login');
                console.log('success', response);
            })
            .catch(error => {
                alert("This email is already existed");
                console.log('error', error);
            });
        }
    }

    return (
        <div className="w-25" style={loginStyle}>
            <h3 className="text-center"> Create Account </h3>
            <form className="form-control">
                <div className="form-group">
                    <label for="username">Username</label>
                    <input type="text" className="form-control" ref={nameRef} id="username" placeholder="Enter username" />
                </div>
                <div className="form-group">
                    <label for="email">Email</label>
                    <input type="email" className="form-control" ref={emailRef} id="email" placeholder="Enter email" />
                </div>
                <div className="form-group">
                    <label for="password">Password</label>
                    <input type="password" className="form-control" ref={passRef} id="password" placeholder="Enter password" />
                </div>
                <div className="form-group">
                    <label for="repeated-password">Confirm Password</label>
                    <input type="password" className="form-control" ref={repeated_passRef} id="repeated-password" placeholder="Enter password again" />
                </div> <br />
                <button type="button" className="btn btn-primary form-control" onClick={handleRegister}> create </button>
            </form>
        </div>
    )
}

export default RegistrationForm;