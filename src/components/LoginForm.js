import React from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import Auth from './Auth';

function LoginForm(props) {

    const loginStyle = {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)"
    };

    const emailRef = React.createRef();
    const passRef = React.createRef();

    const handleLogin = () => {

        const myObj = {
            email: emailRef.current.value,
            password: passRef.current.value
        };

        axios.post('http://127.0.0.1:8000/api/user/login', myObj)
        .then(response => {
            if(response.data === 'access_denied')
            {
                alert(response.data);
            }
            else
            {
                sessionStorage.setItem("user_id", response.data.user_id);
                Auth.login(() => {
                    sessionStorage.setItem("is_authenticated", true);
                    props.history.push('/public');
                });
            }
            console.log('success', response);
        })
        .catch(error => {
            console.log('error', error);
        });
    }

    return (
        <div className="w-25" style={loginStyle}>
            <h3 className="text-center"> Login Form </h3>
            <form className="form-control">
                <div className="form-group">
                    <label for="email">Email</label>
                    <input type="email" className="form-control" ref={emailRef} id="email" placeholder="Enter email" />
                </div>
                <div className="form-group">
                    <label for="password">Password</label>
                    <input type="password" className="form-control" ref={passRef} id="password" placeholder="Enter password" />
                </div> <br />
                <button type="button" className="btn btn-primary form-control" onClick={handleLogin}> Login </button>
                <div className="text-center mt-3">
                    <span> Doesn't have an account yet ? <Link to="/register"> Register now </Link></span>
                </div>
            </form>
        </div>
    )
}

export default LoginForm;