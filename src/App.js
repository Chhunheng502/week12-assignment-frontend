import Header from './components/Header'
import PublicToDoList from './components/PublicToDoList'
import PrivateToDoList from './components/PrivateToDoList'
import LoginForm from './components/LoginForm'
import RegistrationForm from './components/RegistrationForm';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';

import Auth from './components/Auth';

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/login" component={LoginForm} />
          <Route path="/register" component={RegistrationForm} />
          <div>
            <Header />
            <Switch>
              <Route path="/" exact component={() => <Redirect to="/login"> </Redirect>} />
              <PrivateRoute path="/public" component={PublicToDoList} />
              <PrivateRoute path="/private" component={PrivateToDoList} />
            </Switch>
          </div>
        </Switch>
      </Router>
    </div>
  );
}

export default App;

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        Auth.isAuthenticated || sessionStorage.getItem("is_authenticated") ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
}
